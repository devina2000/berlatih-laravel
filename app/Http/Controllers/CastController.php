<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create() 
    {
        return view('cast.create');
    }   

    public function store(Request $request) 
    {
        $request->validate([
            'nama'=> 'required|unique:casts',
            'umur'=> 'required',
            'bio'=> 'required',
        ]);
        $query =DB::table('casts')->insert([
            "nama"=> $request["nama"],
            "umur"=> $request["umur"],
            "bio"=> $request["bio"],
        ]);

        return redirect('/casts');

    }

    public function index() 
    {
        $casts = DB::table('casts')->get();
        return view('cast.index', compact('casts'));
    }

    public function show($id)
    {
        $casts = DB::table('casts')->where('id', $id)->first();
        return view('cast.show', compact('casts'));
    }

    
public function edit($id)
{
    $casts = DB::table('casts')->where('id', $id)->first();
    return view('cast.edit', compact('casts'));
}

public function update($id, Request $request)
{
    $request->validate([
        'nama' => 'required|unique:casts',
        'umur' => 'required',
        'bio' => 'required',
    ]);

    $query = DB::table('casts')
        ->where('id', $id)
        ->update([
            'nama' => $request["nama"],
            'umur' => $request["umur"],
            'bio' => $request["bio"],
        ]);
    return redirect('/casts');
}

public function destroy($id)
    {
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/casts');
    }

}
