<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() {
        return view('register');
    }

    public function welcome() {
        return view('welcome');
    }

    public function welcome_post(Request $request) {
        $fname = $request['fname'];
        $lname = $request['lname'];
        $nama_lengkap = $fname . " " . $lname;
        return view('welcome1',['nama_lengkap'=> $nama_lengkap]);
    }
}
