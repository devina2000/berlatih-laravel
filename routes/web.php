<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@home');

Route::get('/register', 'AuthController@form');

Route::get('/welcome','AuthController@sapa');

Route::Post('/welcome','AuthController@welcome_post');

Route::get('/master', function (){
    return view('layouts.master');

});

Route::get('/table', function (){
    return view('table');

});

Route::get('/data-table', function (){
    return view('data-table');

});

Route::get('/casts', 'CastController@index');
Route::get('/casts/create', 'CastController@create');
Route::post('/casts', 'CastController@store');
Route::get('/casts/{casts_id}', 'CastController@show');
Route::get('/casts/{casts_id}/edit', 'CastController@edit');
Route::put('/casts/{casts_id}', 'CastController@update');
Route::delete('/casts/{casts_id}', 'CastController@destroy');




