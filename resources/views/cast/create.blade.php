@extends('layouts.master')

@section('title')
Tambah cast
@endsection

@section('content')

<div>
        <form action="/casts" methood="POST">
        @csrf
        <div class="form-group">
            <label for="title">Nama</label>
            <input type="text" class="form-control" name="nama"  id="title" placeholder="masukkan nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Umur</label>
            <input type="text" class="form-control" name="umur"  id="body" placeholder="masukkan umur">
            @error('Umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">biodata</label>
            <input type="text" class="form-control" name="bio"  id="body" placeholder="masukkan bio">
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection
    
